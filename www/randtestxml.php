<?php

$randomMultiplier = rand(0, 800);
$count = 70 * $randomMultiplier;

function test_Math($count) {
		$time_start = microtime(true);
		$mathFunctions = array("abs", "acos", "asin", "atan", "bindec", "floor", "exp", "sin", "tan", "pi", "is_finite", "is_nan", "sqrt");
		foreach ($mathFunctions as $key => $function) {
			if (!function_exists($function)) unset($mathFunctions[$key]);
		}
		for ($i=0; $i < $count; $i++) {
			foreach ($mathFunctions as $function) {
				$r = call_user_func_array($function, array($i));
			}
		}
		return number_format(microtime(true) - $time_start, 3);
}

$testResult = test_Math($count);

echo "Math test dla $randomMultiplier trwa : " . $testResult;

//+++++++++++++++++++ TWORZENIE XMLa +++++++++++++++++++++++++++++++++++++++++++++

//tworzenie nazwy pliku XML w-g bieżącej daty
$logName = "/tmp/log-" . date('Y-m-d').".xml"; //  /var/log/...

//tworzenie dokumentu XMl
$xml = new DOMDocument('1.0', 'UTF-8');
$xml->formatOutput = true;
$xml->preserveWhiteSpace = false;

//sprawdzenie czy dzisiejszy dokuemnt (o tej nazwie) już istnieje
if ( $xml->load($logName) ) {
	$xml_root = $xml->getElementsByTagName('logfile')->item(0); //jeśeli tak, to dopisujemy do istniejącego tagu 'logfile'
} else {
	$xml_root = $xml->createElement('logfile'); //jeśli nie, tworzymy tag 'logfile'
	$xml_root = $xml->appendChild( $xml_root ); //i dołączamy go do dokumentu
}

$xml_request = $xml->createElement('request'); //tworzymy tag request (per wywołanie)
$xml_request = $xml_root->appendChild( $xml_request ); //dołączamy jako child dokumentu

$xml_date = $xml->createElement('date', date("Y-m-d H:i:s") ); //tworzymy tag 'date' z bierzęcą datą
$xml_date = $xml_request->appendChild( $xml_date ); //dołączamy jako child tagu 'request'

$xml_value = $xml->createElement('value', $randomMultiplier ); //tworzymy tag 'value' z wartością z randoma
$xml_value = $xml_request->appendChild( $xml_value ); //dołączamy jako child tagu 'request'

$xml_time = $xml->createElement('time', $testResult ); //tworzymy tag 'time' z czasem ładowania strony (obliczenia)
$xml_time = $xml_request->appendChild( $xml_time ); //dołączamy jako child tagu 'request'

$xml->save($logName); //zapisujemy w pliku

//+++++++++++++++++++++++++ XML UTWORZONY +++++++++++++++++++++++++++++++++++++++

/*

#POZOSTAŁE FUNKCJE MOŻLIWE DO WYKORZYSTANIA

function test_StringManipulation($count = 13000) {
		$time_start = microtime(true);
		$stringFunctions = array("addslashes", "chunk_split", "metaphone", "strip_tags", "md5", "sha1", "strtoupper", "strtolower", "strrev", "strlen", "soundex", "ord");
		foreach ($stringFunctions as $key => $function) {
			if (!function_exists($function)) unset($stringFunctions[$key]);
		}
		$string = "the quick brown fox jumps over the lazy dog";
		for ($i=0; $i < $count; $i++) {
			foreach ($stringFunctions as $function) {
				$r = call_user_func_array($function, array($string));
			}
		}
		return number_format(microtime(true) - $time_start, 3);
}

echo "String manipulation test : " . test_StringManipulation();
echo "\r\n";

function test_Loops($count = 1900000) {
		$time_start = microtime(true);
		for($i = 0; $i < $count; ++$i);
		$i = 0; while($i < $count) ++$i;
		return number_format(microtime(true) - $time_start, 3);
}

echo "Loops test : " . test_Loops();
echo "\r\n";



function test_IfElse($count = 9000000) {
		$time_start = microtime(true);
		for ($i=0; $i < $count; $i++) {
			if ($i == -1) {
			} elseif ($i == -2) {
			} else if ($i == -3) {
			}
		}
		return number_format(microtime(true) - $time_start, 3);
}

function test_Math($count = 140000) {
		$time_start = microtime(true);
		$mathFunctions = array("abs", "acos", "asin", "atan", "bindec", "floor", "exp", "sin", "tan", "pi", "is_finite", "is_nan", "sqrt");
		foreach ($mathFunctions as $key => $function) {
			if (!function_exists($function)) unset($mathFunctions[$key]);
		}
		for ($i=0; $i < $count; $i++) {
			foreach ($mathFunctions as $function) {
				$r = call_user_func_array($function, array($i));
			}
		}
		return number_format(microtime(true) - $time_start, 3);
}


function test_StringManipulation($count = 130000) {
		$time_start = microtime(true);
		$stringFunctions = array("addslashes", "chunk_split", "metaphone", "strip_tags", "md5", "sha1", "strtoupper", "strtolower", "strrev", "strlen", "soundex", "ord");
		foreach ($stringFunctions as $key => $function) {
			if (!function_exists($function)) unset($stringFunctions[$key]);
		}
		$string = "the quick brown fox jumps over the lazy dog";
		for ($i=0; $i < $count; $i++) {
			foreach ($stringFunctions as $function) {
				$r = call_user_func_array($function, array($string));
			}
		}
		return number_format(microtime(true) - $time_start, 3);
}


function test_Loops($count = 19000000) {
		$time_start = microtime(true);
		for($i = 0; $i < $count; ++$i);
		$i = 0; while($i < $count) ++$i;
		return number_format(microtime(true) - $time_start, 3);
}

*/

?>
